## 2.0.2

- 发布2.0.2正式版本

## 2.0.1-rc.1

- drawCanvas方法修改逻辑，解决二维码水平不居中问题和底部被遮挡问题

## 2.0.1-rc.0

- 修复不兼容API9问题

## 2.0.1

- qr-code-generator:ArkTs语法适配

## 2.0.0

1.适配DevEco Studio: 3.1Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm

## 1.0.1

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## 1.0.0

1. 二维码生成器
2. 支持按照二维码Model 2标准对所有40个版本（尺寸）和所有4个纠错级别进行编码


